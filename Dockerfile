FROM mcr.microsoft.com/dotnet/aspnet:7.0-alpine AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build

WORKDIR /Source
COPY ["./Source/TodoList.Api/TodoList.Api.csproj", "TodoList.Api/"]
COPY ["./Source/TodoList.Models/TodoList.Models.csproj", "TodoList.Models/"]
COPY ["./Source/TodoListBlazorWasm/TodoListBlazorWasm.csproj", "TodoListBlazorWasm/"]
RUN dotnet restore "TodoList.Api/TodoList.Api.csproj"
RUN dotnet restore "TodoList.Models/TodoList.Models.csproj"
RUN dotnet restore "TodoListBlazorWasm/TodoListBlazorWasm.csproj"
COPY . .
WORKDIR "/Source/Source/TodoList.Api"
RUN dotnet build "TodoList.Api.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "TodoList.Api.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "TodoList.Api.dll"]